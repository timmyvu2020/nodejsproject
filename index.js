const express = require('express');
const mysql = require('mysql');
const port = process.env.PORT || 3000;
const app = express();
const hbs = require("express-handlebars");
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nodejsdb'
}); 
let table = null;
db.connect((err) => {
    if(err){
        throw err;
    } 
    console.log('MySQL connected');
});
app.set('views', './views');
app.set('view engine', 'hbs');
app.engine(
    "hbs",
    hbs({
      defaultLayout: "",
      extname: ".hbs",
    })
  );
app.use(express.urlencoded({
    extended: true
  }))
let profile = {
    username: null,
    clicks: 0
};
app.get('/', (req, res) => {
    if(profile.username){
        res.render('home', profile);
    } else {
        res.render('signin');       
    }
    
})
app.post('/signin', (req, res) => {
    profile.username = req.body.username
    createDB();
    createTable();
    updateTable();
    res.redirect('/');
 });
app.post('/increment', (req, res) => {
    profile.clicks+=1;
    updateClick();
    res.redirect('/')
} )
const createDB = (req, res) => {
    let sql = "CREATE DATABASE IF NOT EXISTS nodejsdb";
    db.query(sql, (err, result) => {
        if(err) throw err;
        // console.log(result);
    });
}
const createTable = (req, res) => {
    let sql = "CREATE TABLE IF NOT EXISTS cookie(name VARCHAR(255), clicks int, PRIMARY KEY(name))";
    db.query(sql, (err, result) => {
        if(err) throw err;
        // console.log(result);
    })
}
const updateTable = () => {
    let sql = `INSERT IGNORE INTO cookie (name, clicks) VALUES ('${profile.username}', ${profile.clicks})`
    db.query(sql, (err, results) => {
        if(err) throw err;
    })
}
const updateClick = () => {
    let sql = `UPDATE cookie SET clicks = ${profile.clicks} WHERE name='${profile.username}'`;
    db.query(sql, (err, results) => {
        if(err) throw err;
    })
}
app.listen(port, () => {
    console.log(`Server started on ${port}`);
});